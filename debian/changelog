fonts-spleen (2.1.0-1) unstable; urgency=medium

  * New upstream version.
  * Bump standards version to 4.7.0.

 -- Alex Myczko <tar@debian.org>  Fri, 16 Aug 2024 08:34:08 +0000

fonts-spleen (2.0.2-1) unstable; urgency=medium

  * New upstream version.
  * d/copyright: bump years.

 -- Gürkan Myczko <tar@debian.org>  Wed, 03 Jan 2024 15:39:39 +0100

fonts-spleen (2.0.1-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Fri, 01 Dec 2023 15:31:52 +0100

fonts-spleen (2.0.0-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Mon, 12 Jun 2023 08:43:35 +0200

fonts-spleen (1.9.3-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <tar@debian.org>  Mon, 17 Apr 2023 07:30:27 +0200

fonts-spleen (1.9.2-1) experimental; urgency=medium

  * New upstream version.
  * Bump standards version to 4.6.2.
  * Update maintainer address.
  * d/copyright: bump years.

 -- Gürkan Myczko <tar@debian.org>  Mon, 20 Feb 2023 13:59:46 +0100

fonts-spleen (1.9.1-1) unstable; urgency=medium

  * New upstream version.
  * Upload to unstable.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 09 Sep 2021 12:54:38 +0200

fonts-spleen (1.9.0-1) experimental; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 18 Mar 2021 12:16:41 +0100

fonts-spleen (1.8.2-1) unstable; urgency=medium

  * New upstream version.
  * Bump debhelper version to 13, drop d/compat.
  * d/upstream/metadata: added.
  * d/control:
    - added Rules-Requires-Root.
    - improved long description.
  * d/copyright:
    - update copyright years.
    - added Upstream-Contact.
  * d/watch: drop template part.
  * d/patches/add-vector-font-of-dunkelstern-fork: set forwarded url.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 05 Nov 2020 08:59:19 +0100

fonts-spleen (1.7.0-2) unstable; urgency=medium

  * Build from source.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 10 Apr 2020 15:31:29 +0200

fonts-spleen (1.7.0-1) unstable; urgency=medium

  * New upstream version.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Mon, 17 Feb 2020 08:14:29 +0100

fonts-spleen (1.6.0-2) unstable; urgency=medium

  * Bump standards version to 4.5.0.
  * Add Vcs fields.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Fri, 07 Feb 2020 09:18:54 +0100

fonts-spleen (1.6.0-1) unstable; urgency=medium

  * New upstream version.
  * Also install the original bitmap font.
  * Bump standards version to 4.4.1.

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Tue, 03 Dec 2019 07:05:40 +0100

fonts-spleen (1.0.4-2) unstable; urgency=medium

  * Team upload.
  * No-change source-only upload to allow testing migration.

 -- Boyuan Yang <byang@debian.org>  Wed, 13 Nov 2019 16:11:28 -0500

fonts-spleen (1.0.4-1) unstable; urgency=medium

  * Initial release. (Closes: #923788)

 -- Gürkan Myczko <gurkan@phys.ethz.ch>  Thu, 28 Mar 2019 12:50:26 +0100
